const gulp = require('gulp');
const del = require('del');
const htmlmin = require('gulp-htmlmin');
const minify = require('gulp-minify');
const csso = require('gulp-csso');
const eslint = require('gulp-eslint');
const gulpIf = require('gulp-if');

function defaultTask(cb) {
  console.log('Please use the following gulp tasks: clean, build');
  cb();
}

function clean() {
  return del('./dist', {
    force: true
  });
}

function pages() {
  return gulp.src(['./lib/views/*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist/views'));
}

function styles() {
  return gulp.src('./lib/styles/*.css')
    .pipe(csso())
    .pipe(gulp.dest('./dist/styles'));
}

function esLint() {
  return gulp.src(['./lib/assets/js/*.js','./lib/scripts/**/*.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .on('error', function(err) {
      console.log('Run \'gulp -- fix\' in terminal to fix these errors'); 
      process.exit();
    });
}

function isFixed(file) {
  return file.eslint !== null && file.eslint.fixed;
}

function fix() {
  return gulp.src(['./lib/assets/js/*.js','./lib/scripts/**/*.js'])
    .pipe(eslint({fix:true}))
    .pipe(eslint.format())
    .pipe(gulpIf(isFixed, gulp.dest(function (file) {
      return file.base;
    })))
    .pipe(eslint.failAfterError());
}

function minifyTask() {
  return gulp.src('./lib/scripts/**/*.js')
    .pipe(minify({
      ext: {
        min: '.js'
      },
      noSource: true,
    }))
    .pipe(gulp.dest('./dist/scripts'));
}

function copyDist() {
  gulp.src('./lib/_locales/**/*').pipe(gulp.dest('./dist/_locales/'));
  gulp.src('./lib/common/*').pipe(gulp.dest('./dist/common/'));
  gulp.src('./lib/assets/**/*').pipe(gulp.dest('./dist/assets/'));
  return gulp.src('./lib/manifest.json').pipe(gulp.dest('./dist/'));
}

const build = gulp.series(clean, gulp.parallel(pages, styles, esLint, minifyTask, copyDist));

exports.default = defaultTask;
exports.clean = clean;
exports.pages = pages;
exports.styles = styles;
exports.esLint = esLint;
exports.fix = fix;
exports.minify = minifyTask;
exports.copyDist = copyDist;
exports.build = build;
